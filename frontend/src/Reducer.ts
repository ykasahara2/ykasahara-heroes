import { reducerWithInitialState } from 'typescript-fsa-reducers';
import * as actions from './Actions';
import * as State from './State';

export const initialState: State.State = {
    heroID: '',
    heroes: [],
    users: [],
    fanLetters: [],
    fanLetterMessage: '',
};

export const reducer = reducerWithInitialState(initialState)
    .case(actions.setHeroID, (state, payload) => ({ ...state, heroID: payload }))
    .case(actions.setHeroes, (state, payload) => ({ ...state, heroes: payload }))
    .case(actions.setUsers, (state, payload) => ({ ...state, users: payload }))
    .case(actions.setFanLetters, (state, payload) => ({ ...state, fanLetters: payload }))
    .case(actions.inputFanLetterMessage, (state, payload) => ({ ...state, fanLetterMessage: payload }));
