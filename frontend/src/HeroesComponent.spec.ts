import { createStore, Store } from 'redux';
import * as data from '../bin/data';
import * as actions from './Actions';
import * as HttpClient from './HttpClient';
import * as MapStateToProps from './MapStateToProps';
import * as Reducer from './Reducer';
import * as State from './State';

describe('reducer', () => {
    it('setHeroID', () => {
        const newState = Reducer.reducer(Reducer.initialState, actions.setHeroID('hero1'));
        expect(newState.heroID).toEqual('hero1');
    });

    it('setHeroes', () => {
        const newState = Reducer.reducer(
            Reducer.initialState,
            actions.setHeroes([
                { id: 'hero5', name: 'ドクター・ストレンジ' },
                { id: 'hero6', name: 'マイティー・ソー' },
            ]),
        );
        expect(newState.heroes[0].name).toEqual('ドクター・ストレンジ');
        expect(newState.heroes[1].name).toEqual('マイティー・ソー');
    });
});

describe('mapStateToProps', () => {
    it('heroName', () => {
        const props = MapStateToProps.mapStateToProps({
            ...Reducer.initialState,
            heroID: 'hero1',
            heroes: data.getAllData().heroes,
        });
        expect(props.heroName).toEqual('スーパーマン');
    });

    it('fanLetterProps', () => {
        const props = MapStateToProps.mapStateToProps({
            ...Reducer.initialState,
            heroID: 'hero1',
            heroes: data.getAllData().heroes,
        });
        expect(props.fanLetterProps.length).toEqual(0);
    });
});

describe('thunk', () => {
    it('postFanLetter', async () => {
        const store: Store<State.State> = createStore<State.State, any, any, any>(Reducer.reducer, {
            heroID: 'hero1',
            fanLetterMessage: 'message1',
            fanLetters: [],
            heroes: [],
            users: [],
        });

        await actions.postFanLetter()(store.dispatch, store.getState, {
            userID: 'user1',
            httpClient: new MocHttpClient(null),
            getNow: () => new Date(2019, 7, 1),
        });

        const actual = store.getState().fanLetters[0];

        const ex: State.FanLetter = {
            heroID: 'hero1',
            fanLetterID: 'new_fan_letter_id',
            userID: 'user1',
            message: 'message1',
            timestamp: new Date(2019, 7, 1),
        };

        expect(actual).toEqual(ex);
    });
});

class MocHttpClient extends HttpClient.HttpClient {
    public async postFanLetter(userID: string, heroID: string, message: string, timestamp: Date): Promise<string> {
        return 'new_fan_letter_id';
    }
}
