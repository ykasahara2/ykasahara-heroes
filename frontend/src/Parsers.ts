import * as RestTypes from './RestTypes';
import * as State from './State';

export function parseUsers(users: RestTypes.User[]): State.User[] {
    return users.map(u => parseUser(u));
}

export function parseUser(user: RestTypes.User): State.User {
    return {
        id: user.id,
        name: user.name,
    };
}

export function parseHeroes(heroes: RestTypes.Hero[]): State.Hero[] {
    return heroes.map(h => parseHero(h));
}

export function parseHero(hero: RestTypes.Hero): State.Hero {
    return {
        id: hero.id,
        name: hero.name,
    };
}

export function parseFanLetters(fanLetters: RestTypes.FanLetter[]): State.FanLetter[] {
    return fanLetters.map(fl => parseFanLetter(fl));
}

export function parseFanLetter(fanLetter: RestTypes.FanLetter): State.FanLetter {
    return {
        fanLetterID: fanLetter.fan_letter_id,
        heroID: fanLetter.hero_id,
        userID: fanLetter.user_id,
        message: fanLetter.message,
        timestamp: new Date(fanLetter.timestamp),
    };
}
