import { Optional } from 'typescript-optional';
import * as Models from './Models';
import * as Props from './Props';
import * as State from './State';

export function mapStateToProps(state: State.State): Props.Props {
    const userModels: Models.UserModels = Models.modelingUsers(state.users);
    const heroModels: Models.HeroModels = Models.modelingHeroes(state.heroes, state.fanLetters);

    const heroModel: Optional<Models.HeroModel> = heroModels.get(state.heroID);

    const matchFanLetters = heroModel.isEmpty()
        ? []
        : heroModel.get().fanLetters.map(
              (fl): Props.FanLetterProps => ({
                  fanLetterID: fl.fanLetterID,
                  userID: fl.userID,
                  userName: userModels.getName(fl.userID).orElse(''),
                  message: fl.message,
              }),
          );

    return {
        heroes: state.heroes,
        heroName: heroModel.map(h => h.name).orElse(''),
        fanLetterProps: matchFanLetters,
    };
}
