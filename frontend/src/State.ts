export interface State {
    heroID: string;
    heroes: Hero[];
    users: User[];
    fanLetters: FanLetter[];
    fanLetterMessage: string;
}

export interface Hero {
    id: string;
    name: string;
}

export interface User {
    id: string;
    name: string;
}

export interface FanLetter {
    fanLetterID: string;
    userID: string;
    heroID: string;
    message: string;
    timestamp: Date;
}
