import { Optional } from 'typescript-optional';
import * as State from './State';

export class UserModels extends Array<State.User> {
    public getName(userID: string): Optional<string> {
        return Optional.ofNullable(this.find(u => u.id === userID)).map(u => u.name);
    }
}

export class HeroModels extends Array<HeroModel> {
    public get(heroID: string): Optional<HeroModel> {
        return Optional.ofNullable(this.find(h => h.heroID === heroID));
    }
}

export class HeroModel {
    constructor(public heroID: string, public name: string, public fanLetters: State.FanLetter[]) {}
}

export function modelingUsers(allUsers: State.User[]): UserModels {
    return new UserModels(...allUsers);
}

export function modelingHeroes(heroes: State.Hero[], fanLetters: State.FanLetter[]): HeroModels {
    return new HeroModels(
        ...heroes.map(h => {
            const matchFanLetters = fanLetters.filter(fl => fl.heroID === h.id);
            return new HeroModel(h.id, h.name, matchFanLetters);
        }),
    );
}
