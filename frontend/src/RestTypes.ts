export interface Hero {
    id: string;
    name: string;
}

export interface User {
    id: string;
    name: string;
}

export interface FanLetter {
    fan_letter_id: string;
    hero_id: string;
    user_id: string;
    message: string;
    timestamp: number;
}

export interface PostFanLetter extends Omit<FanLetter, 'fan_letter_id'> {}
