import * as State from './State';

export interface Props {
    heroes: State.Hero[];
    heroName: string;
    fanLetterProps: FanLetterProps[];
}

export interface FanLetterProps {
    fanLetterID: string;
    userID: string;
    userName: string;
    message: string;
}
