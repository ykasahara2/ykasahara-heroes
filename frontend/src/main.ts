import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { RootComponent } from './HeroesComponent';

export async function main() {
    const element = document.createElement('react-root');
    document.body.appendChild(element);
    const heroesElement = React.createElement(RootComponent);
    ReactDOM.render(heroesElement, element);
}
