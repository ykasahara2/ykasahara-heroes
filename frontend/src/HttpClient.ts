import { AxiosInstance } from 'axios';
import * as Parsers from './Parsers';
import * as RestTypes from './RestTypes';
import * as State from './State';

export class HttpClient {
    constructor(private axios: AxiosInstance) {}

    public async getHeroes(): Promise<State.Hero[]> {
        return Parsers.parseHeroes((await this.axios.get('/api/heroes')).data.results);
    }

    public async getUsers(): Promise<State.User[]> {
        return Parsers.parseUsers((await this.axios.get('/api/users')).data.results);
    }

    public async getFanLetters(): Promise<State.FanLetter[]> {
        return Parsers.parseFanLetters((await this.axios.get('/api/fan-letters')).data.results);
    }

    public async postFanLetter(userID: string, heroID: string, message: string, timestamp: Date): Promise<string> {
        const body: RestTypes.PostFanLetter = {
            user_id: userID,
            hero_id: heroID,
            message: message,
            timestamp: timestamp.getTime(),
        };
        const res = await this.axios.post('/api/fan-letters', body);
        return res.data.results;
    }
}
