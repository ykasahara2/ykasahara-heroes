import Axios from 'axios';
import * as React from 'react';
import { connect, Provider } from 'react-redux';
import { applyMiddleware, bindActionCreators, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import * as actions from './Actions';
import { HttpClient } from './HttpClient';
import * as MapStateToProps from './MapStateToProps';
import * as Props from './Props';
import * as Reducer from './Reducer';
import * as State from './State';

declare let window: any;

type AppProps = Props.Props & typeof actions;

const HeroesComponent = connect(
    MapStateToProps.mapStateToProps,
    (dispatch: any): typeof actions => bindActionCreators(actions, dispatch),
)(
    class extends React.Component<AppProps> {
        public render() {
            return (
                <div>
                    <ul>
                        {this.props.heroes.map(h => (
                            <li key={h.id} onClick={_ => this.props.setHeroID(h.id)}>
                                {h.name}
                            </li>
                        ))}
                    </ul>
                    <p>{this.props.heroName}</p>
                    <table>
                        <tbody>
                            {this.props.fanLetterProps.map(fl => (
                                <tr key={fl.fanLetterID}>
                                    <td>{fl.userName}</td>
                                    <td>{fl.message}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                    <textarea cols={30} rows={10} onChange={e => this.props.inputFanLetterMessage(e.target.value)} />
                    <button onClick={_ => this.props.postFanLetter()}>送信</button>
                </div>
            );
        }
    },
);

export const RootComponent: React.SFC = props => {
    const composeEnhancers =
        typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
            ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
            : compose;
    const httpClient = new HttpClient(Axios.create());
    const extraArgument = { httpClient: httpClient, getNow: () => new Date(), userID: 'user1' };
    const enhancer = composeEnhancers(applyMiddleware(thunk.withExtraArgument(extraArgument)));
    const store = createStore<State.State, any, any, any>(Reducer.reducer, enhancer);

    httpClient.getHeroes().then(heroes => store.dispatch(actions.setHeroes(heroes)));
    httpClient.getUsers().then(users => store.dispatch(actions.setUsers(users)));
    httpClient.getFanLetters().then(fanLetters => store.dispatch(actions.setFanLetters(fanLetters)));

    return (
        <Provider store={store}>
            <HeroesComponent />
        </Provider>
    );
};
