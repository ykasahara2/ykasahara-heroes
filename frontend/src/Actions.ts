import * as Redux from 'redux';
import actionCreatorFactory from 'typescript-fsa';
import * as HttpClient from './HttpClient';
import * as State from './State';

const actionCreator = actionCreatorFactory();

export const setHeroID = actionCreator<string>('setHeroID');
export const setHeroes = actionCreator<State.Hero[]>('setHeroes');
export const setUsers = actionCreator<State.User[]>('setUsers');
export const setFanLetters = actionCreator<State.FanLetter[]>('setFanLetters');
export const inputFanLetterMessage = actionCreator<string>('inputFanLetterMessage');

export function postFanLetter() {
    return async (
        dispatch: Redux.Dispatch,
        getState: () => State.State,
        extraArgument: { httpClient: HttpClient.HttpClient; getNow: () => Date; userID: string },
    ) => {
        const fanLetterMessage = getState().fanLetterMessage;
        const heroID = getState().heroID;

        const now = extraArgument.getNow();
        const userID = extraArgument.userID;

        const dummyFanLeterID = '#NEW_FAN_LETTER';
        const dummyFanLetter: State.FanLetter = {
            fanLetterID: dummyFanLeterID,
            message: fanLetterMessage,
            timestamp: now,
            userID: userID,
            heroID: heroID,
        };
        dispatch(setFanLetters([...getState().fanLetters, dummyFanLetter]));

        const fanLetterID = await extraArgument.httpClient.postFanLetter(userID, heroID, fanLetterMessage, now);
        const newFanLetters: State.FanLetter[] = [
            ...getState().fanLetters.filter(f => f.fanLetterID !== dummyFanLeterID),
            { ...dummyFanLetter, fanLetterID: fanLetterID },
        ];
        dispatch(setFanLetters(newFanLetters));
    };
}
