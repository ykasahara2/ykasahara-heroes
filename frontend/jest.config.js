module.exports = {
    'moduleFileExtensions': [
        'ts',
        'tsx',
        'js',
        'jsx',
        'json',
        'node'
    ],
    'testRegex': '(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$',
    'transform': {
        '.(ts|tsx)': './node_modules/ts-jest/preprocessor.js',
    },
}