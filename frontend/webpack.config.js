const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const webpack = require('webpack');
const copyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        app: './src/index.js',
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].bundle.js',
    },
    module: {
        rules: [
            {
                exclude: /node_modules/,
                test: /\.scss$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'Hot Module Replacement',
            template: 'src/index.html',
        }),
        new webpack.HotModuleReplacementPlugin(),

    ],
    devServer: {
        hot: true,
        host: 'localhost',
        port: 8080,
        proxy: {
            '/api': {
                target: `http://localhost:8081`,
                pathRewrite: {'^/api': ''}
            },
            '/ws': {
                target: `ws://localhost:8081`,
                ws: true
            }
        }
    },
};