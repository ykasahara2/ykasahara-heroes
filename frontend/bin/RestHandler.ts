import { Router } from 'express';
import * as RestTypes from '../src/RestTypes';
import { AllData } from './data';
import { response, webSocketSend } from './main';

export function setUpRestHandler(router: Router, allData: AllData) {
    router.get('/', (req, res) => {
        res.json('OK');
    });

    router.get('/heroes', (req, res) => {
        res.json(response(allData.heroes));
    });

    router.get('/users', (req, res) => {
        res.json(response(allData.users));
    });

    router.get('/fan-letters', (req, res) => {
        res.json(response(allData.fanLetters));
    });

    router.post('/fan-letters', (req, res) => {
        const body = req.body as RestTypes.PostFanLetter;
        const id = Math.random()
            .toString()
            .slice(-8);
        const fanLetter: RestTypes.FanLetter = { ...body, fan_letter_id: id };
        allData.fanLetters.push(fanLetter);
        res.status(201);
        res.json(response(id));
    });

    setInterval(() => webSocketSend('hello'), 1000);
}
