import * as RestTypes from '../src/RestTypes';

export interface AllData {
    heroes: RestTypes.Hero[];
    users: RestTypes.User[];
    fanLetters: RestTypes.FanLetter[];
}

export function getAllData(): AllData {
    return {
        heroes: [
            { id: 'hero1', name: 'スーパーマン' },
            { id: 'hero2', name: 'キャプテン・アメリカ' },
            { id: 'hero3', name: 'アイアンマン' },
            { id: 'hero4', name: 'スパイダーマン' },
            { id: 'hero5', name: 'バットマン' },
        ],
        users: [
            { id: 'user1', name: 'ユーザー１' },
            { id: 'user2', name: 'ユーザー２' },
            { id: 'user3', name: 'ユーザー３' },
            { id: 'user4', name: 'ユーザー４' },
        ],
        fanLetters: [
            {
                fan_letter_id: 'fanLetter1',
                hero_id: 'hero1',
                user_id: 'user1',
                message: '大好きです。',
                timestamp: Date.now(),
            },
            {
                fan_letter_id: 'fanLetter2',
                hero_id: 'hero1',
                user_id: 'user2',
                message: 'かっこいい',
                timestamp: Date.now(),
            },
            {
                fan_letter_id: 'fanLetter3',
                hero_id: 'hero2',
                user_id: 'user2',
                message: '応援しています。',
                timestamp: Date.now(),
            },
            {
                fan_letter_id: 'fanLetter4',
                hero_id: 'hero3',
                user_id: 'user3',
                message: '頑張ってください',
                timestamp: Date.now(),
            },
        ],
    };
}
