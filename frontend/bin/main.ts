import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as express from 'express';
import * as WebSocketServer from 'ws';
import * as RestTypes from '../src/RestTypes';
import { getAllData } from './data';
import { setUpRestHandler } from './RestHandler';

const websockets: any[] = [];
const router = express.Router();
const app = express();
const allData = getAllData();

setUpRestHandler(router, allData);

export function response(body: any) {
    return {
        results: body,
    };
}

app.use(cookieParser());
app.use(bodyParser.json());
app.use('/', router);

const server = app.listen(8081);

const wss = new WebSocketServer.Server({ server: server });
wss.on('connection', (ws: any, req: any) => {
    websockets.push(ws);
});

export function webSocketSend(data: any) {
    for (const ws of websockets) {
        if (ws.readyState === 1 /*OPEN https://developer.mozilla.org/ja/docs/Web/API/WebSocket */) {
            ws.send('do update');
        }
    }
}
