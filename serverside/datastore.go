package serverside

import (
	"net/http"
	"net/url"
	"time"

	"github.com/mjibson/goon"
	"google.golang.org/appengine"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/taskqueue"
)

type (
	User struct {
		ID   string `datastore:"-" goon:"id"` // Data:UserID
		Name string `datastore:"Name"`
	}
	Hero struct {
		ID   string `datastore:"ID" goon:"id"`
		Name string `datastore:"Name"`
	}
	FanLetter struct {
		ID        string    `datastore:"-" goon:"id"`
		UserID    string    `datastore:"UserID"`
		HeroID    string    `datastore:"HeroID"`
		Message   string    `datastore:"Message,noindex"`
		Timestamp time.Time `datastore:"Timestamp"`
	}
	CacheHero struct {
		HeroID         string `datastore:"-" goon:"id"`
		Hero           Hero   `datastore:"Hero,noindex"`
		FanLetterCount int    `datastore:"FanLetterCount"`
	}
)

type (
	UserJson struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	}
	HeroJson struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	}
	FanLetterJson struct {
		ID        string `json:"id"`
		UserID    string `json:"user_id"`
		HeroID    string `json:"hero_id"`
		Message   string `json:"message"`
		Timestamp int64  `json:"timestamp"`
	}
	PostFanLetterJson struct {
		UserID    string `json:"user_id"`
		HeroID    string `json:"hero_id"`
		Message   string `json:"message"`
		Timestamp int64  `json:"timestamp"`
	}
)

type DataObjects struct {
	Users      []*User
	Heroes     []*CacheHero
	FanLetters []*FanLetter
}

type PostFanLetterParam struct {
	RequestBody PostFanLetterJson
}

type GetPopularHeroesParam struct {
	Limit int
}

type Response struct {
	StatusCode      int
	ResponseBody    interface{}
	WriteObjects    []interface{}
	CacheOperations []interface{}
}

type CacheHeroOperate struct {
	HeroID string
}

func GetUsers(r *http.Request) []*User {
	var users []*User
	q := datastore.NewQuery("User")
	g := goon.NewGoon(r)
	g.GetAll(q, &users)
	return users
}

func GetUser(r *http.Request, userID string) (*User, error) {
	user := &User{ID: userID}
	g := goon.NewGoon(r)
	if err := g.Get(user); err != nil {
		return nil, err
	}
	return user, nil
}

func GetHeroes(r *http.Request) []*Hero {
	var heroes []*Hero
	q := datastore.NewQuery("Hero")
	g := goon.NewGoon(r)
	g.GetAll(q, &heroes)
	return heroes
}

func GetPopularHeroes(p GetPopularHeroesParam, models Models, service Service) Response {
	return Response{
		StatusCode:   http.StatusOK,
		ResponseBody: assignHeroesFromHeroModel(models.HeroModels.Take(p.Limit)),
	}
}

func GetHero(r *http.Request, heroID string) (*Hero, error) {
	hero := &Hero{ID: heroID}
	g := goon.NewGoon(r)
	if err := g.Get(hero); err != nil {
		return nil, err
	}
	return hero, nil
}

func GetFanLetters(r *http.Request) []*FanLetter {
	var fanLetters []*FanLetter
	q := datastore.NewQuery("FanLetter")
	g := goon.NewGoon(r)
	g.GetAll(q, &fanLetters)
	return fanLetters
}

func GetFanLettersToHero(r *http.Request, heroID string) []*FanLetter {
	var fanLetters []*FanLetter
	q := datastore.NewQuery("FanLetter").Filter("HeroID =", heroID)
	g := goon.NewGoon(r)
	g.GetAll(q, &fanLetters)
	return fanLetters
}

func GetCacheHero(r *http.Request, heroID string) (*CacheHero, error) {
	cacheHero := &CacheHero{HeroID: heroID}
	g := goon.NewGoon(r)
	if err := g.Get(cacheHero); err != nil {
		return nil, err
	}
	return cacheHero, nil
}

func GetPopularHeroesInLimit(r *http.Request, limit int) []*CacheHero {
	var cacheHeroes []*CacheHero
	q := datastore.NewQuery("CacheHero").Order("-FanLetterCount").Limit(limit)
	g := goon.NewGoon(r)
	g.GetAll(q, &cacheHeroes)
	return cacheHeroes
}

func PutFanLetter(r *http.Request, fanLetter FanLetter) {
	g := goon.NewGoon(r)
	g.Put(&fanLetter)
}

func PutCacheHero(r *http.Request, cacheHero CacheHero) {
	g := goon.NewGoon(r)
	g.Put(&cacheHero)
}

// models にはデータベース上の全データが入っている前提
func PostFanLetter(p PostFanLetterParam, models Models, service Service) Response {
	if !models.UserModels.isUserExists(p.RequestBody.UserID) {
		return Response{
			StatusCode:   http.StatusNotFound,
			ResponseBody: "該当するユーザーは存在しません",
		}
	}

	if !models.HeroModels.isHeroExists(p.RequestBody.HeroID) {
		return Response{
			StatusCode:   http.StatusNotFound,
			ResponseBody: "該当するヒーローは存在しません",
		}
	}

	newFanLetter := FanLetter{
		UserID:    p.RequestBody.UserID,
		HeroID:    p.RequestBody.HeroID,
		Message:   p.RequestBody.Message,
		ID:        service.RandomString(),
		Timestamp: service.Now(),
	}

	return Response{
		StatusCode:      http.StatusCreated,
		ResponseBody:    newFanLetter.ID,
		WriteObjects:    []interface{}{newFanLetter},
		CacheOperations: []interface{}{CacheHeroOperate{HeroID: newFanLetter.HeroID}},
	}
}

func PostCacheHeroes(heroes []*Hero, fanLetters []*FanLetter) Response {
	var writeObjects []interface{}
	for _, hero := range heroes {
		newCacheHero := CacheHero{
			HeroID:         hero.ID,
			Hero:           *hero,
			FanLetterCount: countFanLetter(hero.ID, fanLetters),
		}
		writeObjects = append(writeObjects, newCacheHero)
	}

	return Response{
		StatusCode:   http.StatusOK,
		WriteObjects: writeObjects,
	}
}

func PostCacheHero(hero *Hero, fanLetters []*FanLetter) Response {
	newCacheHero := CacheHero{
		HeroID:         hero.ID,
		Hero:           *hero,
		FanLetterCount: len(fanLetters),
	}
	writeObjects := []interface{}{newCacheHero}

	return Response{
		StatusCode:   http.StatusOK,
		WriteObjects: writeObjects,
	}
}

func DeleteAllCacheHero(r *http.Request) {
	q := datastore.NewQuery("CacheHero").KeysOnly()
	g := goon.NewGoon(r)
	keys, _ := g.GetAll(q, nil)
	for _, key := range keys {
		g.Delete(key)
	}
}

func DeleteCacheHero(r *http.Request, heroID string) error {
	cacheHero, err := GetCacheHero(r, heroID)
	if err != nil {
		return err
	}
	g := goon.NewGoon(r)
	if err := g.Delete(cacheHero); err != nil {
		return err
	}
	return nil
}

func countFanLetter(heroID string, fanLetters []*FanLetter) int {
	var result int
	for _, fanLetter := range fanLetters {
		if heroID == fanLetter.HeroID {
			result++
		}
	}
	return result
}

func assignUseres(users []*User) []UserJson {
	results := []UserJson{}
	for _, u := range users {
		ju := UserJson{
			ID:   u.ID,
			Name: u.Name,
		}
		results = append(results, ju)
	}
	return results
}

func assignHeroes(heroes []*Hero) []HeroJson {
	results := []HeroJson{}
	for _, h := range heroes {
		jh := HeroJson{
			ID:   h.ID,
			Name: h.Name,
		}
		results = append(results, jh)
	}
	return results
}

func assignHeroesFromHeroModel(heroModels HeroModels) []HeroJson {
	results := []HeroJson{}
	for _, hm := range heroModels {
		jh := HeroJson{
			ID:   hm.ID,
			Name: hm.Name,
		}
		results = append(results, jh)
	}
	return results
}

func assignFanLetteres(fanLetters []*FanLetter) []FanLetterJson {
	results := []FanLetterJson{}
	for _, f := range fanLetters {
		jf := FanLetterJson{
			ID:      f.ID,
			UserID:  f.UserID,
			HeroID:  f.HeroID,
			Message: f.Message,
		}
		results = append(results, jf)
	}
	return results
}

func WriteAll(r *http.Request, writeObjects []interface{}) {
	for _, o := range writeObjects {
		switch o := o.(type) {
		case FanLetter:
			PutFanLetter(r, o)
		case CacheHero:
			PutCacheHero(r, o)
		}
	}
}

func CacheOperations(r *http.Request, operations []interface{}) {
	ctx := appengine.NewContext(r)

	for _, o := range operations {
		switch o := o.(type) {
		case CacheHeroOperate:
			parameters := url.Values{}
			t := taskqueue.NewPOSTTask("/cache/heroes/"+o.HeroID, parameters)
			taskqueue.Add(ctx, t, "")
		}
	}
}
