package serverside

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/mjibson/goon"
)

func init() {
	gin.SetMode(gin.DebugMode)
	router := gin.New()
	http.Handle("/", handleRouter(router))
}

func handleRouter(router *gin.Engine) *gin.Engine {
	router.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "HELLO")
	})

	router.GET("/api/heroes", func(c *gin.Context) {
		sort := c.Query("sort")
		if sort == "fan_letter_count_desc" {
			limit, err := strconv.Atoi(c.Query("limit"))
			if err != nil {
				c.JSON(http.StatusBadRequest, err.Error())
				return
			}

			inquiries := []Inquiry{
				InquiryPopularHero{Limit: limit},
			}
			dataObjects := Fetch(c.Request, inquiries)
			res := GetPopularHeroes(GetPopularHeroesParam{Limit: limit}, d2m(dataObjects), &ServiceImple{})
			c.JSON(res.StatusCode, res.ResponseBody)
		} else {
			heroes := GetHeroes(c.Request)
			c.JSON(http.StatusOK, assignHeroes(heroes))
		}
	})

	router.GET("/api/users", func(c *gin.Context) {
		users := GetUsers(c.Request)
		c.JSON(http.StatusOK, assignUseres(users))
	})

	router.GET("/api/fan_letters", func(c *gin.Context) {
		fanLetters := GetFanLetters(c.Request)
		c.JSON(http.StatusOK, assignFanLetteres(fanLetters))
	})

	router.POST("/api/fan_letters", func(c *gin.Context) {
		var fanLetterBody PostFanLetterJson
		c.BindJSON(&fanLetterBody)

		p := PostFanLetterParam{
			RequestBody: fanLetterBody,
		}

		inquiries := []Inquiry{
			InquiryGetUser{UserID: fanLetterBody.UserID},
			InquiryGetCacheHero{HeroID: fanLetterBody.HeroID},
		}
		dataObjects := Fetch(c.Request, inquiries)

		res := PostFanLetter(p, d2m(dataObjects), &ServiceImple{})

		WriteAll(c.Request, res.WriteObjects)
		CacheOperations(c.Request, res.CacheOperations)

		c.JSON(res.StatusCode, res.ResponseBody)
	})

	router.GET("/api/init", func(c *gin.Context) {
		DataStoreInit(c.Request)
	})

	router.POST("/cache/heroes", func(c *gin.Context) {
		DeleteAllCacheHero(c.Request)

		heroes := GetHeroes(c.Request)
		fanLetters := GetFanLetters(c.Request)
		res := PostCacheHeroes(heroes, fanLetters)

		WriteAll(c.Request, res.WriteObjects)

		c.JSON(res.StatusCode, res.ResponseBody)
	})

	router.POST("/cache/heroes/:hero_id", func(c *gin.Context) {
		heroID := c.Param("hero_id")
		hero, err := GetHero(c.Request, heroID)
		if err != nil {
			c.JSON(http.StatusBadRequest, "該当するヒーローは存在しません")
			return
		}

		DeleteCacheHero(c.Request, hero.ID)

		fanLetters := GetFanLettersToHero(c.Request, heroID)
		res := PostCacheHero(hero, fanLetters)

		WriteAll(c.Request, res.WriteObjects)

		c.JSON(res.StatusCode, res.ResponseBody)
	})

	return router
}

func DataStoreInit(r *http.Request) {
	g := goon.NewGoon(r)
	g.PutMulti(&[]User{
		{ID: "user1", Name: "ユーザー1"},
		{ID: "user2", Name: "ユーザー2"},
		{ID: "user3", Name: "ユーザー3"},
		{ID: "user4", Name: "ユーザー4"},
	})
	g.PutMulti(&[]Hero{
		{ID: "hero1", Name: "スーパーマン"},
		{ID: "hero2", Name: "キャプテン・アメリカ"},
		{ID: "hero3", Name: "アイアンマン"},
		{ID: "hero4", Name: "スパイダーマン"},
		{ID: "hero5", Name: "バットマン"},
	})
	g.PutMulti(&[]FanLetter{
		{ID: "fanLetter1", HeroID: "hero1", UserID: "user1", Message: "大好きです。"},
		{ID: "fanLetter2", HeroID: "hero1", UserID: "user2", Message: "かっこいい"},
		{ID: "fanLetter3", HeroID: "hero2", UserID: "user2", Message: "応援しています。"},
		{ID: "fanLetter4", HeroID: "hero3", UserID: "user3", Message: "頑張ってください"},
	})
}
