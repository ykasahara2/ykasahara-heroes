package serverside

import "time"

type UserModels []UserModel

type UserModel struct {
	ID   string
	Name string
}

func (userModels UserModels) isUserExists(userID string) bool {
	for _, userModel := range userModels {
		if userModel.ID == userID {
			return true
		}
	}
	return false
}

type HeroModels []HeroModel

type HeroModel struct {
	ID   string
	Name string
}

func (heroModels HeroModels) isHeroExists(heroID string) bool {
	for _, heroModel := range heroModels {
		if heroModel.ID == heroID {
			return true
		}
	}
	return false
}

func (heroModels HeroModels) Take(limit int) HeroModels {
	if len(heroModels) > limit {
		return heroModels[:limit]
	}
	return heroModels
}

type FanLetterModels []FanLetterModel

type FanLetterModel struct {
	ID        string
	UserID    string
	HeroID    string
	Message   string
	Timestamp time.Time
}

type Models struct {
	UserModels      UserModels
	HeroModels      HeroModels
	FanLetterModels FanLetterModels
}

func d2m(dataObjects DataObjects) Models {
	return Models{
		UserModels:      du2um(dataObjects.Users),
		HeroModels:      dch2hm(dataObjects.Heroes),
		FanLetterModels: dfl2flm(dataObjects.FanLetters),
	}
}

func du2um(users []*User) UserModels {
	var userModels UserModels
	for _, user := range users {
		userModels = append(userModels, UserModel{
			ID:   user.ID,
			Name: user.Name,
		})
	}
	return userModels
}

func dch2hm(cacheHeroes []*CacheHero) HeroModels {
	var heroModels HeroModels
	for _, cacheHero := range cacheHeroes {
		heroModels = append(heroModels, HeroModel{
			ID:   cacheHero.Hero.ID,
			Name: cacheHero.Hero.Name,
		})
	}
	return heroModels
}

func dfl2flm(fanLetters []*FanLetter) FanLetterModels {
	var fanLetterModels FanLetterModels
	for _, fanLetter := range fanLetters {
		fanLetterModels = append(fanLetterModels, FanLetterModel{
			ID:        fanLetter.ID,
			UserID:    fanLetter.UserID,
			HeroID:    fanLetter.HeroID,
			Message:   fanLetter.Message,
			Timestamp: fanLetter.Timestamp,
		})
	}
	return fanLetterModels
}
