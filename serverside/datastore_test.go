package serverside

import (
	"net/http"
	"testing"
	"time"
)

var users = []*User{
	{ID: "user1", Name: "ユーザー1"},
	{ID: "user2", Name: "ユーザー2"},
	{ID: "user3", Name: "ユーザー3"},
	{ID: "user4", Name: "ユーザー4"},
}
var heroes = []*Hero{
	{ID: "hero1", Name: "スーパーマン"},
	{ID: "hero2", Name: "キャプテン・アメリカ"},
	{ID: "hero3", Name: "アイアンマン"},
	{ID: "hero4", Name: "スパイダーマン"},
	{ID: "hero5", Name: "バットマン"},
}
var fanLetters = []*FanLetter{
	{ID: "fanLetter1", HeroID: "hero1", UserID: "user1", Message: "大好きです。"},
	{ID: "fanLetter2", HeroID: "hero1", UserID: "user2", Message: "かっこいい"},
	{ID: "fanLetter3", HeroID: "hero2", UserID: "user2", Message: "応援しています。"},
	{ID: "fanLetter4", HeroID: "hero3", UserID: "user3", Message: "頑張ってください"},
}
var cacheHeroes = []*CacheHero{
	{HeroID: "hero1", Hero: *heroes[0], FanLetterCount: 2},
	{HeroID: "hero2", Hero: *heroes[1], FanLetterCount: 1},
	{HeroID: "hero3", Hero: *heroes[2], FanLetterCount: 1},
	{HeroID: "hero4", Hero: *heroes[3], FanLetterCount: 0},
	{HeroID: "hero5", Hero: *heroes[4], FanLetterCount: 0},
}
var fanLetterService = ServiceMock{
	mockRandom: "new_fan_letter_id",
	mockTime:   time.Date(2019, time.April, 1, 10, 0, 0, 0, time.UTC),
}
var cacheHeroService = ServiceMock{
	mockRandom: "new_cache_hero_id",
	mockTime:   time.Date(2019, time.April, 1, 10, 0, 0, 0, time.UTC),
}

func Test_PostFanLetter_UserNotFound(t *testing.T) {
	p := PostFanLetterParam{
		RequestBody: PostFanLetterJson{
			UserID:  "user5",
			HeroID:  "hero1",
			Message: "応援のコメント",
		},
	}
	res := PostFanLetter(p, d2m(DataObjects{Users: users, Heroes: cacheHeroes}), fanLetterService)

	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("該当ユーザーがいないのに404にならない, %d", res.StatusCode)
	}
}

func Test_PostFanLetterHerorNotFound(t *testing.T) {
	p := PostFanLetterParam{
		RequestBody: PostFanLetterJson{
			UserID:  "user1",
			HeroID:  "hero6",
			Message: "応援のコメント",
		},
	}
	res := PostFanLetter(p, d2m(DataObjects{Users: users, Heroes: cacheHeroes}), fanLetterService)

	if res.StatusCode != http.StatusNotFound {
		t.Fatalf("該当ヒーローがいないのに404にならない, %d", res.StatusCode)
	}
}

func Test_PostFanLetter_Created(t *testing.T) {
	p := PostFanLetterParam{
		RequestBody: PostFanLetterJson{
			UserID:  "user1",
			HeroID:  "hero1",
			Message: "応援のコメント",
		},
	}
	res := PostFanLetter(p, d2m(DataObjects{Users: users, Heroes: cacheHeroes}), fanLetterService)
	writeObject := res.WriteObjects[0].(FanLetter)

	if writeObject.ID != "new_fan_letter_id" {
		t.Fatalf("IDが違う, %s", writeObject.ID)
	}
	if writeObject.Timestamp.Format("2006/01/02 15:04:05") != "2019/04/01 10:00:00" {
		t.Fatalf("時刻が違う, %s", writeObject.Timestamp.Format("2006/01/02 15:04:05"))
	}
	if writeObject.Message != "応援のコメント" {
		t.Fatalf("メッセージが違う, %s", writeObject.Message)
	}
}

func Test_PostCacheHeroes_Created(t *testing.T) {
	res := PostCacheHeroes(heroes, fanLetters)
	writeObject := res.WriteObjects[0].(CacheHero)

	if writeObject.HeroID != heroes[0].ID {
		t.Fatalf("IDが違う, %s", writeObject.HeroID)
	}
	if writeObject.Hero.Name != heroes[0].Name {
		t.Fatalf("ヒーローが違う, %s", writeObject.Hero.Name)
	}
	if writeObject.FanLetterCount != 2 {
		t.Fatalf("ファンレター件数が違う, %d", writeObject.FanLetterCount)
	}
}

func Test_PostCacheHeroe_Created(t *testing.T) {
	hero1 := heroes[0]
	fanLettersToHero := []*FanLetter{
		{ID: "fanLetter1", HeroID: "hero1", UserID: "user1", Message: "大好きです。"},
		{ID: "fanLetter2", HeroID: "hero1", UserID: "user2", Message: "かっこいい"},
	}
	res := PostCacheHero(hero1, fanLettersToHero)
	writeObject := res.WriteObjects[0].(CacheHero)

	if writeObject.HeroID != hero1.ID {
		t.Fatalf("IDが違う, %s", writeObject.HeroID)
	}
	if writeObject.Hero.Name != hero1.Name {
		t.Fatalf("ヒーローが違う, %s", writeObject.Hero.Name)
	}
	if writeObject.FanLetterCount != 2 {
		t.Fatalf("ファンレター件数が違う, %d", writeObject.FanLetterCount)
	}
}

func Test_GetPopularHeroes_Take(t *testing.T) {
	p := GetPopularHeroesParam{Limit: 6}
	dataObjects := DataObjects{Heroes: cacheHeroes}
	res := GetPopularHeroes(p, d2m(dataObjects), cacheHeroService)

	popularHeroes := res.ResponseBody.([]HeroJson)
	if len(popularHeroes) != 5 {
		t.Fatalf("人気ヒーローの件数が Limit と異なる, %d", len(popularHeroes))
	}
}
