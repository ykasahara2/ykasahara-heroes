package serverside

import (
	"net/http"
	"reflect"
)

type (
	Inquiry interface {
		fetch(r *http.Request) []interface{}
	}

	InquiryGetUser struct {
		UserID string
	}

	InquiryGetHero struct {
		HeroID string
	}

	InquiryGetCacheHero struct {
		HeroID string
	}

	InquiryPopularHero struct {
		Limit int
	}
)

func (inquiry InquiryGetUser) fetch(r *http.Request) []interface{} {
	var results []interface{}
	user, err := GetUser(r, inquiry.UserID)
	if err == nil {
		results = append(results, user)
	}
	return results
}

func (inquiry InquiryGetHero) fetch(r *http.Request) []interface{} {
	var results []interface{}
	hero, err := GetHero(r, inquiry.HeroID)
	if err == nil {
		results = append(results, hero)
	}
	return results
}

func (inquiry InquiryGetCacheHero) fetch(r *http.Request) []interface{} {
	var results []interface{}
	cacheHero, err := GetCacheHero(r, inquiry.HeroID)
	if err == nil {
		results = append(results, cacheHero)
	}
	return results
}

func (inquiry InquiryPopularHero) fetch(r *http.Request) []interface{} {
	var results []interface{}
	cacheHeroes := GetPopularHeroesInLimit(r, inquiry.Limit)
	for _, cacheHero := range cacheHeroes {
		results = append(results, cacheHero)
	}
	return results
}

func Fetch(r *http.Request, inquiries []Inquiry) DataObjects {
	var dataObjects DataObjects
	for _, inquiry := range inquiries {
		dataObjects = appendEach(dataObjects, inquiry.fetch(r))
	}
	return dataObjects
}

func appendEach(objects DataObjects, list []interface{}) DataObjects {
	for _, item := range list {
		hpv := reflect.ValueOf(&objects)
		t := hpv.Elem().Type()
		n := t.NumField()
		for fieldIndex := 0; fieldIndex < n; fieldIndex++ {
			field := t.Field(fieldIndex)
			fieldType := field.Type
			if fieldType.Kind() != reflect.Slice {
				continue
			}
			match := fieldType.Elem().Elem() == reflect.TypeOf(item).Elem()
			if !match {
				continue
			}
			l := reflect.Append(hpv.Elem().Field(fieldIndex), reflect.ValueOf(item))
			hpv.Elem().Field(fieldIndex).Set(l)
		}
	}
	return objects
}
