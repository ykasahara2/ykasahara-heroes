package serverside

import (
	"testing"
)

func Test_ObjectAppend(t *testing.T) {
	d := DataObjects{}
	list := []interface{}{&User{ID: "user1"}, &User{ID: "user2"}, &Hero{ID: "hero1"}, &FanLetter{ID: "fanLetter1"}}
	actual := appendEach(d, list)

	if len(actual.Users) != 2 {
		t.Fatalf("ユーザが2ではなく %d", len(actual.Users))
	}

	if actual.Users[0].ID != "user1" {
		t.Fatalf("actual.Users[0].ID が user1 ではなく %s", actual.Users[0].ID)
	}
}
