package serverside

import (
	"crypto/rand"
	"encoding/binary"
	"strconv"
	"time"
)

type (
	Service interface {
		Now() time.Time
		RandomString() string
	}

	ServiceImple struct{}

	ServiceMock struct {
		mockRandom string
		mockTime   time.Time
	}
)

func (s ServiceImple) Now() time.Time {
	return time.Now()
}

func (s ServiceImple) RandomString() string {
	var n uint64
	binary.Read(rand.Reader, binary.LittleEndian, &n)
	return strconv.FormatUint(n, 36)
}

func (s ServiceMock) Now() time.Time {
	return s.mockTime
}

func (s ServiceMock) RandomString() string {
	return s.mockRandom
}
